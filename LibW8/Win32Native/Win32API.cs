#pragma warning disable SA1310 // Field names must not contain underscore
namespace Win32Native
{
    using System;
    using System.Diagnostics;
    using System.Runtime.InteropServices;

    public class Win32API
    {
        private const int GWL_HWNDPARENT = -8;

        internal enum WindowShowStyle : uint
        {
            Hide,
            ShowNormal,
            ShowMinimized,
            ShowMaximized,
            Maximize = 3u,
            ShowNormalNoActivate,
            Show,
            Minimize,
            ShowMinNoActivate,
            ShowNoActivate,
            Restore,
            ShowDefault,
            ForceMinimized
        }

        internal enum WindowHandleInsertAfter
        {
            HWND_TOP,
            HWND_BOTTOM,
            HWND_TOPMOST = -1,
            HWND_NOTOPMOST = -2
        }

        [Flags]
        internal enum WindowPosFlags : uint
        {
            SWP_NOSIZE = 1u,
            SWP_NOMOVE = 2u,
            SWP_NOZORDER = 4u,
            SWP_NOREDRAW = 8u,
            SWP_NOACTIVATE = 0x10,
            SWP_DRAWFRAME = 0x20,
            SWP_FRAMECHANGED = 0x20,
            SWP_SHOWWINDOW = 0x40,
            SWP_HIDEWINDOW = 0x80,
            SWP_NOCOPYBITS = 0x100,
            SWP_NOOWNERZORDER = 0x200,
            SWP_NOREPOSITION = 0x200,
            SWP_NOSENDCHANGING = 0x400,
            SWP_DEFERERASE = 0x2000,
            SWP_ASYNCWINDOWPOS = 0x4000
        }

        public static void ShowWindowWithoutActivation(IntPtr handle)
        {
            WindowPosFlags windowPosFlags =
                WindowPosFlags.SWP_NOSIZE
                | WindowPosFlags.SWP_NOMOVE
                | WindowPosFlags.SWP_NOACTIVATE
                | WindowPosFlags.SWP_NOZORDER
                | WindowPosFlags.SWP_SHOWWINDOW
                ;
            if (handle != IntPtr.Zero)
            {
                NativeMethods.SetWindowPos(
                    handle, (IntPtr)0, 0, 0, 0, 0, windowPosFlags);
            }
        }

        public static void HideWindow(IntPtr handle)
        {
            WindowPosFlags windowPosFlags =
                WindowPosFlags.SWP_NOSIZE
                | WindowPosFlags.SWP_NOMOVE
                | WindowPosFlags.SWP_NOACTIVATE
                | WindowPosFlags.SWP_NOZORDER
                | WindowPosFlags.SWP_HIDEWINDOW;
            if (!(handle == IntPtr.Zero))
            {
                Win32API.NativeMethods.SetWindowPos(
                    handle, (IntPtr)0, 0, 0, 0, 0, windowPosFlags);
            }
        }

        public static void BringWindowToTopActivation(IntPtr handle)
        {
            WindowPosFlags windowPosFlags =
                WindowPosFlags.SWP_NOSIZE
                | WindowPosFlags.SWP_NOMOVE
                | WindowPosFlags.SWP_SHOWWINDOW
                | WindowPosFlags.SWP_NOSENDCHANGING;
            if (!(handle == IntPtr.Zero))
            {
                NativeMethods.SetWindowPos(
                    handle, (IntPtr)0, 0, 0, 0, 0, windowPosFlags);
            }
        }

        public static void SetForceForegroundWindow(IntPtr targetHandle)
        {
            if (!(targetHandle == IntPtr.Zero))
            {
                uint num = 0u;
                uint windowThreadProcessId =
                    NativeMethods
                    .GetWindowThreadProcessId(targetHandle, out num);
                uint windowThreadProcessId2 =
                    NativeMethods
                    .GetWindowThreadProcessId(
                        NativeMethods.GetForegroundWindow(), out num);
                NativeMethods.SetForegroundWindow(targetHandle);
                if (windowThreadProcessId == windowThreadProcessId2)
                {
                    NativeMethods.BringWindowToTop(targetHandle);
                }
                else
                {
                    NativeMethods.AttachThreadInput(
                        windowThreadProcessId, windowThreadProcessId2, true);
                    try
                    {
                        NativeMethods.BringWindowToTop(targetHandle);
                    }
                    finally
                    {
                        NativeMethods.AttachThreadInput(
                            windowThreadProcessId, windowThreadProcessId2, false);
                    }
                }
            }
        }

        public static IntPtr GetPrevGameWindowHandle(string processName)
        {
            IntPtr result = IntPtr.Zero;

            var currentProcess = Process.GetCurrentProcess();
            Process[] processesByName = Process.GetProcessesByName(processName);
            foreach (Process process in processesByName)
            {
                IntPtr windowHandle = process.MainWindowHandle;
                if (!(windowHandle == IntPtr.Zero) && process != currentProcess)
                {
                    result = windowHandle;
                    break;
                }
            }

            return result;
        }

        public static IntPtr GetGameWindowHandle(string processName)
        {
            IntPtr result = IntPtr.Zero;
            Process[] processesByName = Process.GetProcessesByName(processName);
            foreach (Process process in processesByName)
            {
                IntPtr mainWindowHandle = process.MainWindowHandle;
                if (!(mainWindowHandle == IntPtr.Zero))
                {
                    result = mainWindowHandle;
                }
            }

            return result;
        }

        public static(RECT rect, POINT point) GetGameWindowInfo(IntPtr handle)
        {
            var rect = default(RECT);
            //// ゲームウインドウの位置とサイズを取得。
            var result = NativeMethods.GetClientRect(handle, out rect);

            if (result == true)
            {
                //// ゲームウィンドウのスクリーン上の位置を取得。
                var screenPoint = new POINT { X = rect.Left, Y = rect.Top };
                NativeMethods.ClientToScreen(handle, out screenPoint);

                return (rect, screenPoint);
            }
            else
            {
                //// 取得失敗
                var point = default(POINT);
                return (rect, point);
            }
        }

        public static void Restore(IntPtr handle)
        {
            if (!(handle == IntPtr.Zero) && Win32API.NativeMethods.IsIconic(handle))
            {
                NativeMethods.ShowWindow(handle, Win32API.WindowShowStyle.Restore);
            }
        }

        public static IntPtr SetWindowLongPtr(HandleRef hWnd, IntPtr dwNewLong)
        {
            if (IntPtr.Size == 8)
            {
                return NativeMethods.SetWindowLongPtr64(hWnd, GWL_HWNDPARENT, dwNewLong);
            }
            else
            {
                return new IntPtr(NativeMethods.SetWindowLong32(hWnd, GWL_HWNDPARENT, dwNewLong.ToInt32()));
            }
        }

        /// <summary>
        /// 指定したhWndの親Window handleを返す。
        /// </summary>
        /// <param name="hWnd">Window handle</param>
        /// <returns>親Window handle</returns>
        public static IntPtr GetParentWindowHandle(IntPtr hWnd)
        {
            return GetWindowLongPtr(hWnd, GWL_HWNDPARENT);
        }

        /// <summary>
        /// 指定した Window handle の nIndex で指定した情報を返す。
        /// </summary>
        /// <param name="hWnd">Window handle</param>
        /// <param name="nIndex">取得する情報の種類</param>
        /// <returns>取得した情報</returns>
        private static IntPtr GetWindowLongPtr(IntPtr hWnd, int nIndex)
        {
            if (IntPtr.Size == 8)
            {
                return NativeMethods.GetWindowLongPtr64(hWnd, nIndex);
            }
            else
            {
                return new IntPtr(NativeMethods.GetWindowLong32(hWnd.ToInt32(), nIndex));
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;

            public int Y;

            public POINT(int x, int y)
            {
                this.X = x;
                this.Y = y;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;

            public int Top;

            public int Right;

            public int Bottom;
        }

        internal class NativeMethods
        {
            [DllImport("user32.dll")]
            internal static extern bool AttachThreadInput(uint idAttach, uint idAttachTo, bool fAttach);

            [DllImport("user32.dll", SetLastError = true)]
            internal static extern bool BringWindowToTop(IntPtr hWnd);

            [DllImport("user32.dll")]
            internal static extern bool ClientToScreen(IntPtr hwnd, out POINT lpPoint);

            [DllImport("user32.dll")]
            internal static extern bool GetClientRect(IntPtr hwnd, out RECT lpRect);

            [DllImport("user32.dll")]
            internal static extern IntPtr GetForegroundWindow();

            [DllImport("user32.dll", SetLastError = true)]
            internal static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            internal static extern bool IsIconic(IntPtr hWnd);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            internal static extern bool SetForegroundWindow(IntPtr hWnd);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            internal static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, WindowPosFlags uFlags);

            [DllImport("user32.dll")]
            internal static extern bool ShowWindow(IntPtr hWnd, WindowShowStyle nCmdShow);

            [DllImport("User32.Dll")]
            internal static extern IntPtr GetDesktopWindow();

            [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
            internal static extern int SetWindowLong32(HandleRef hWnd, int nIndex, int dwNewLong);

            [DllImport("user32.dll", EntryPoint = "SetWindowLongPtr")]
            internal static extern IntPtr SetWindowLongPtr64(HandleRef hWnd, int nIndex, IntPtr dwNewLong);

            [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
            internal static extern int GetWindowLong32(int hWnd, int nIndex);

            [DllImport("user32.dll", EntryPoint = "GetWindowLongPtr")]
            internal static extern IntPtr GetWindowLongPtr64(IntPtr hWnd, int nIndex);
        }
    }
}
#pragma warning restore SA1310 // Field names must not contain underscore
