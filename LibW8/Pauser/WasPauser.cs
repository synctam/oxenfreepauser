﻿namespace LibW8.Pauser
{
    using System;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using LibW8.WatchDoc;
    using Win32Native;

    public class WasPauser
    {
        /// <summary>
        /// ゲームの状態を変更する。
        /// </summary>
        /// <param name="processName">プロセス名</param>
        /// <returns>ゲームの状態</returns>
        public static
            (WasGameWatcher.NGameStatus status,
            Win32API.POINT point, IntPtr hWnd) Toggle(string processName)
        {
            var result = GetGameStatus(processName);
            switch (result.status)
            {
                case WasGameWatcher.NGameStatus.Inactive:
                    //// ゲームが起動していない時は、状態を維持。
                    return (WasGameWatcher.NGameStatus.Inactive, result.point, result.handle);
                case WasGameWatcher.NGameStatus.Pause:
                    //// 一時停止状態の時は実行状態に変更する。
                    Resume(processName);
                    return (WasGameWatcher.NGameStatus.Run, result.point, result.handle);
                case WasGameWatcher.NGameStatus.Run:
                    //// 実行状態の時は一時停止状態にする。
                    Pause();
                    return (WasGameWatcher.NGameStatus.Pause, result.point, result.handle);
                default:
                    Debug.Assert(true, "Unknown status/");
                    return (WasGameWatcher.NGameStatus.Inactive, result.point, result.handle);
            }
        }

        /// <summary>
        /// ゲームのプロセスを調査し、ゲームの状態を返す。
        /// </summary>
        /// <param name="processName">プロセス名</param>
        /// <returns>ゲームの状態,Windowサイズ,Window位置,WindowHandle</returns>
        public static(WasGameWatcher.NGameStatus status, Win32API.POINT point, IntPtr handle)
            GetGameStatus(string processName)
        {
            var point = default(Win32API.POINT);

            var hWnd = Win32API.GetGameWindowHandle(processName);
            if (hWnd == IntPtr.Zero)
            {
                //// ゲームのプロセスが存在しない。
                return (WasGameWatcher.NGameStatus.Inactive, point, hWnd);
            }
            else
            {
                var result = Win32API.GetGameWindowInfo(hWnd);

                //// ゲームは起動中
                var activeWindowHandle = Win32API.NativeMethods.GetForegroundWindow();
                if (hWnd == activeWindowHandle)
                {
                    //// ゲームはアクティブ
                    return (WasGameWatcher.NGameStatus.Run, result.point, hWnd);
                }
                else
                {
                    //// ゲームはインアクティブ
                    return (WasGameWatcher.NGameStatus.Pause, result.point, hWnd);
                }
            }
        }

        /// <summary>
        /// DesktopWindowにフォーカスを移動する。
        /// </summary>
        public static void Pause()
        {
            var hWnd = Win32API.NativeMethods.GetDesktopWindow();
            Win32API.SetForceForegroundWindow(hWnd);
        }

        /// <summary>
        /// 指定したプロセス名のウィンドウにフォーカスを移動する。
        /// </summary>
        /// <param name="processName">プロセス名</param>
        public static void Resume(string processName)
        {
            var hWnd = Win32API.GetGameWindowHandle(processName);
            Win32API.SetForceForegroundWindow(hWnd);
            Win32API.BringWindowToTopActivation(hWnd);
        }

        /// <summary>
        /// ゲーム画面上に"一時停止" と表示するために、OSD画面の親をゲーム画面にする。
        /// (OSDウィンドウの親を Game window Handle に変更し、成否を返す)
        /// </summary>
        /// <param name="status">Game status</param>
        /// <param name="defaultParentWindowHandle">OSD default parent Window handle</param>
        /// <param name="osdHandleRef">OSD Window handle ref</param>
        /// <param name="gameWindowHandle">Game window handle</param>
        /// <returns>成否</returns>
        public static bool ChangeWindowParent(
            WasGameWatcher.NGameStatus status,
            IntPtr defaultParentWindowHandle,
            HandleRef osdHandleRef,
            IntPtr gameWindowHandle)
        {
            IntPtr newParentWindowHandle = IntPtr.Zero;
            switch (status)
            {
                case WasGameWatcher.NGameStatus.Inactive:
                    //// 親ウィンドウをデフォルト値に設定する。
                    newParentWindowHandle = defaultParentWindowHandle;
                    break;
                case WasGameWatcher.NGameStatus.Run:
                case WasGameWatcher.NGameStatus.Pause:
                    //// 親ウィンドウをゲームウィンドウに設定する。
                    newParentWindowHandle = gameWindowHandle;
                    break;
                default:
                    throw new Exception($"Unknown error.status({status.ToString()})");
            }

            if (newParentWindowHandle ==
                Win32API.GetParentWindowHandle(osdHandleRef.Handle))
            {
                //// すでに変更済み
                return true;
            }
            else
            {
                //// OSDウィンドウの親を変更する。
                IntPtr result =
                    Win32API.SetWindowLongPtr(osdHandleRef, newParentWindowHandle);
                if (result == newParentWindowHandle)
                {
                    //// 親ウィンドウの変更に成功。
                    return true;
                }
                else
                {
                    //// 親ウィンドウの変更に失敗。
                    return false;
                }
            }
        }
    }
}
