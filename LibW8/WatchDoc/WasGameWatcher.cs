﻿namespace LibW8.WatchDoc
{
    using System;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Windows.Forms;
    using LibW8.Pauser;
    using NHotkey;
    using NHotkey.WindowsForms;
    using Reactive.Bindings;
    using Win32Native;

    /// <summary>
    /// ゲームの監視
    /// 1.ゲームの状態を監視
    /// 2.ゲームの状態に応じホットキーの設定/解除
    /// 3.ゲームの状態をビューに通知。
    /// </summary>
    public class WasGameWatcher
    {
        /// <summary>
        /// [定数] 監視のインターバル
        /// </summary>
        public const int WatchdogInterval = 200;

        /// <summary>
        /// [定数] 監視開始直後の監視開始までの時間
        /// </summary>
        private const int WatchdogPreDelay = 200;

        /// <summary>
        /// 監視するゲームのプロセス名
        /// </summary>
        private string processName = string.Empty;

        /// <summary>
        /// 監視用タイマー
        /// </summary>
        private System.Threading.Timer timer = null;

        /// <summary>
        /// ホットキーの登録状態
        /// </summary>
        private bool isHotHeyRegistered = false;

        private Keys hotKey1 = Keys.Pause;
        private Keys hotKey2 = Keys.F11;

        /// <summary>
        /// OSDウィンドウのデフォルトの親ウィンドウ。
        /// </summary>
        private IntPtr defaultParentWindowHandle = IntPtr.Zero;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="processName">プロセス名</param>
        /// <param name="hotKey1">ホットキー１</param>
        /// <param name="hotKey2">ホットキー２</param>
        /// <param name="osdHandleRef">OSD画面のハンドルのリファレンス</param>
        public WasGameWatcher(
            string processName, Keys hotKey1, Keys hotKey2, HandleRef osdHandleRef)
        {
            this.processName = processName;
            this.hotKey1 = hotKey1;
            this.hotKey2 = hotKey2;
            this.OsdHandleRef = osdHandleRef;
            this.defaultParentWindowHandle =
                Win32API.GetParentWindowHandle(this.OsdHandleRef.Handle);
        }

        /// <summary>
        /// ゲームの状態
        /// </summary>
        public enum NGameStatus
        {
            /// <summary>
            /// ゲームは起動していない
            /// </summary>
            Inactive,

            /// <summary>
            /// ゲームは実行中でアクティブ
            /// </summary>
            Run,

            /// <summary>
            /// ゲームは起動中で非アクティブ
            /// </summary>
            Pause,
        }

        /// <summary>
        /// [通知] ゲームのステータス
        /// </summary>
        public ReactivePropertySlim<NGameStatus> Status { get; } =
            new ReactivePropertySlim<NGameStatus>(NGameStatus.Inactive);

        /// <summary>
        /// ゲーム画面のスクリーン上の位置
        /// </summary>
        public Win32API.POINT Point { get; private set; } = default(Win32API.POINT);

        /// <summary>
        /// OSD画面のハンドル
        /// </summary>
        public HandleRef OsdHandleRef { get; private set; } = default(HandleRef);

        /// <summary>
        /// ゲームの監視を開始する。
        /// </summary>
        public void Start()
        {
            //// タイマーイベント時に実行する処理を登録する。
            TimerCallback callback = _ =>
            {
                //// ゲームの状態を調べ、状態を設定する。
                var result = WasPauser.GetGameStatus(this.processName);
                this.Point = result.point;

                WasPauser.ChangeWindowParent(
                    result.status,
                    this.defaultParentWindowHandle,
                    this.OsdHandleRef,
                    result.handle);

                //// [通知] ゲームの状態を通知する。
                this.Status.Value = result.status;
            };

            //// タイマーを開始する。
            this.timer = new System.Threading.Timer(
                callback, null, WatchdogPreDelay, WatchdogInterval);
        }

        /// <summary>
        /// ゲームの監視を停止する。
        /// </summary>
        public void Stop()
        {
            this.timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        /// <summary>
        /// ゲームを一時停止状態にする。
        /// </summary>
        public void Pause()
        {
            WasPauser.Pause();
        }

        /// <summary>
        /// ゲームを実行時状態にする。
        /// </summary>
        public void Resume()
        {
            WasPauser.Resume(this.processName);
        }

        /// <summary>
        /// 監視を終了する。
        /// </summary>
        public void Close()
        {
            //// タイマーを破棄する。
            using (this.timer)
            {
            }
        }

        /// <summary>
        /// ホットキーを登録する。
        /// </summary>
        public void SetHotKey()
        {
            if (!this.isHotHeyRegistered)
            {
                HotkeyManager.Current.AddOrReplace(
                    "GamePauser1", this.hotKey1, this.OnKeyPress);
                HotkeyManager.Current.AddOrReplace(
                    "GamePauser2", this.hotKey2, this.OnKeyPress);
                this.isHotHeyRegistered = true;
            }
        }

        /// <summary>
        /// ホットキーの登録を解除する。
        /// </summary>
        public void RemoveHotKey()
        {
            try
            {
                if (this.isHotHeyRegistered)
                {
                    HotkeyManager.Current.Remove("GamePauser1");
                    HotkeyManager.Current.Remove("GamePauser2");
                }
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                //// ホットキーが未登録の場合は例外を無視。
            }

            this.isHotHeyRegistered = false;
        }

        /// <summary>
        /// [App event] ホットキーが押された時:ゲームの状態を反転させる。
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void OnKeyPress(object sender, HotkeyEventArgs e)
        {
            WasPauser.Toggle(this.processName);

            e.Handled = true;
        }
    }
}
