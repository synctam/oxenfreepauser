(2019.04.15)

Oxenfree 一時停止ツール Oxenfree Pauser v1.0.0

これは、ホラーアドベンチャーゲーム Oxenfree を一時停止するツールです。

使い方などの詳細は、以下のリンクにあるブログ記事を御覧ください。

「Oxenfree 一時停止ツール」
https://synctam.blogspot.com/2019/04/oxenfree.html


■変更履歴
2019.04.15: v1.0.0 初版公開
