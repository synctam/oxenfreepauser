﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace w8w.Properties {
    using System;
    
    
    /// <summary>
    ///   ローカライズされた文字列などを検索するための、厳密に型指定されたリソース クラスです。
    /// </summary>
    // このクラスは StronglyTypedResourceBuilder クラスが ResGen
    // または Visual Studio のようなツールを使用して自動生成されました。
    // メンバーを追加または削除するには、.ResX ファイルを編集して、/str オプションと共に
    // ResGen を実行し直すか、または VS プロジェクトをビルドし直します。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   このクラスで使用されているキャッシュされた ResourceManager インスタンスを返します。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("w8w.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   すべてについて、現在のスレッドの CurrentUICulture プロパティをオーバーライドします
        ///   現在のスレッドの CurrentUICulture プロパティをオーバーライドします。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Oxenfree 一時停止ツール
        ///
        ///■概要
        ///Oxenfreeは台詞がどんどん流れていくため、英語が苦手な私にはまったく読めません。また、スクリーンショットを取りたくてもタイミングが難しく取りこぼしが出たりします。
        ///そこで、ゲームを一時停止するツールを作りました。
        ///
        ///■使い方
        ///このツールと Oxenfree を起動します。
        ///ゲームをプレイしている最中に一時停止したい場合は、キーボード右上にある &quot;Pause&quot; ボタンを押してください。
        ///そうするとゲームが一時停止します。
        ///もう一度、&quot;Pause&quot; ボタンを押すとゲームが再開されます。
        ///
        ///
        ///
        ///■ツールの仕組み
        ///Oxenfree は、別のウィンドウをアクティブにするとゲームが一時停止するように作られています。
        ///このため、このツールを使わなくても、デスクトップや別のウインドウをアクティブにすればゲームを一時停止できます。
        ///しかし、全画面でプレイしたい場合や、他のウインドウをアクティブにするのが面倒な場合はこのツールが有効だと思います。
        ///
        ///ツールの画面が邪魔であれば、最小化しておくと良いでしょう。
        ///(最小化すると画面右下のタスクト [残りの文字列は切り詰められました]&quot;; に類似しているローカライズされた文字列を検索します。
        /// </summary>
        internal static string Usage {
            get {
                return ResourceManager.GetString("Usage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   (アイコン) に類似した型 System.Drawing.Icon のローカライズされたリソースを検索します。
        /// </summary>
        internal static System.Drawing.Icon W8w {
            get {
                object obj = ResourceManager.GetObject("W8w", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   (アイコン) に類似した型 System.Drawing.Icon のローカライズされたリソースを検索します。
        /// </summary>
        internal static System.Drawing.Icon W8wActive {
            get {
                object obj = ResourceManager.GetObject("W8wActive", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   (アイコン) に類似した型 System.Drawing.Icon のローカライズされたリソースを検索します。
        /// </summary>
        internal static System.Drawing.Icon W8wSleep {
            get {
                object obj = ResourceManager.GetObject("W8wSleep", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
    }
}
