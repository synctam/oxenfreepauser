﻿#pragma warning disable SA1300 // Element must begin with upper-case letter
namespace W8w
{
    using System;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Windows.Forms;
    using LibW8.Pauser;
    using LibW8.WatchDoc;
    using w8w.OsdForm;

    public partial class W8wMainForm : Form
    {
        private const string Title = "Oxenfree一時停止ツール";
        private string processName = string.Empty;
        private WasGameWatcher gameWatcher = null;
        private IDisposable appStateHandle = null;
        private bool isClosing = false;

        /// <summary>
        /// トレイアイコンクリック時のボタンの種類：左クリックの有無。
        /// </summary>
        private MouseButtons trayIconClickButton = MouseButtons.None;
        private OsdForm osdForm = new OsdForm();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public W8wMainForm()
        {
            this.InitializeComponent();
            this.Init();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            this.Text = Title;
            this.Width = 600;
            this.Height = 500;
            this.notifyIcon1.Icon = w8w.Properties.Resources.W8w;
            this.processName = w8w.Properties.Settings.Default.GameProcessName;

            //// リソースから説明文を取り出し表示。
            string usage = w8w.Properties.Resources.Usage;
            this.textBox1.Text = usage;
            this.textBox1.Select(0, 0);

            //// 各種アイコンの説明
            this.pictureBoxIdol.Image = w8w.Properties.Resources.W8w.ToBitmap();
            this.pictureBoxIdol.Width = 32;
            this.pictureBoxIdol.Height = 32;
            this.labelIdol.Text = "Oxenfreeが起動されていない";

            this.pictureBoxActive.Image = w8w.Properties.Resources.W8wActive.ToBitmap();
            this.pictureBoxActive.Width = 32;
            this.pictureBoxActive.Height = 32;
            this.labelActive.Text = "Oxenfreeが動作中";

            this.pictureBoxPause.Image = w8w.Properties.Resources.W8wSleep.ToBitmap();
            this.pictureBoxPause.Width = 32;
            this.pictureBoxPause.Height = 32;
            this.labelPause.Text = "Oxenfreeが一時停止中";

            //// "一時停止"を表示するフォームを表示。
            //// フォームは透明で、文字のみが表示される。
            //// 初期状態は文字列が空のため見えない。
            this.osdForm.Show();

            //// ゲーム監視オブジェクトの作成
            var handleRef = new HandleRef(this.osdForm, this.osdForm.Handle);
            this.gameWatcher = new WasGameWatcher(
                this.processName,
                w8w.Properties.Settings.Default.HotKey1,
                w8w.Properties.Settings.Default.HotKey2,
                handleRef);

            //// [通知受信設定] ゲームの状態が変更されたときに実行する処理を登録
            this.appStateHandle = this.gameWatcher.Status.Subscribe(state =>
            {
                if (this.isClosing)
                {
                    //// フォームのクローズ処理開始後は、イベントの処理は行わない。
                    return;
                }

                switch (state)
                {
                    case WasGameWatcher.NGameStatus.Inactive:
                        //// ゲームが終了した
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new Action(this.gameWatcher.RemoveHotKey));
                            this.Invoke(new Action(this.OnGameInactive));
                        }
                        else
                        {
                            this.gameWatcher.RemoveHotKey();
                            this.OnGameInactive();
                        }

                        break;
                    case WasGameWatcher.NGameStatus.Pause:
                        //// ゲームが一時停止した
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new Action(this.gameWatcher.SetHotKey));
                            this.Invoke(new Action(this.OnGamePause));
                        }
                        else
                        {
                            this.gameWatcher.SetHotKey();
                            this.OnGamePause();
                        }

                        break;
                    case WasGameWatcher.NGameStatus.Run:
                        //// ゲームが実行状態になった
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new Action(this.gameWatcher.SetHotKey));
                            this.Invoke(new Action(this.OnGameResume));
                        }
                        else
                        {
                            this.gameWatcher.SetHotKey();
                            this.OnGameResume();
                        }

                        break;
                    default:
                        Debug.Assert(true, "Unknown status.");
                        break;
                }
            });
        }

        /// <summary>
        /// [Form event] フォームロード時
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void W8wMainForm_Load(object sender, EventArgs e)
        {
            //// ゲームの監視を開始する。
            this.gameWatcher.Start();
        }

        /// <summary>
        /// [Form event] フォームを閉じる前
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void W8wMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //// クローズ処理開始を宣言。
            //// クローズ処理後に、Watchdogイベントが発生すると、
            //// フォームがDispose中と判断され "ObjectDisposedException" 例外が
            //// 発生する事があるため。
            this.isClosing = true;

            //// 登録済みの処理を解除
            this.appStateHandle.Dispose();

            //// ゲームの監視を停止する。
            this.gameWatcher.Resume();
            this.gameWatcher.Stop();

            //// Watchdog のスレッドが完了するのを待つ。
            Thread.Sleep(WasGameWatcher.WatchdogInterval * 2);

            //// ゲームの監視を終了する。
            this.gameWatcher.Close();

            e.Cancel = false;
        }

        /// <summary>
        /// [Form event] タスクトレイアイコン クリック時
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            //// 左クリック時は、クリック処理、右クリック時は、メニュー表示。
            if (this.trayIconClickButton == MouseButtons.Left)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    //// 最小化している時は、タスクバーを復旧し、ウィンドウを表示する。
                    this.ShowInTaskbar = true;
                    this.WindowState = FormWindowState.Normal;
                }
                else
                {
                    this.Activate();
                }
            }
        }

        /// <summary>
        /// [Form event] タスクトレイアイコンから終了。
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void mnuExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// [Form event] クリックボタンの状態を保存。
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void notifyIcon1_MouseUp(object sender, MouseEventArgs e)
        {
            this.trayIconClickButton = e.Button;
        }

        /// <summary>
        /// [Form event] リサイズ時
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void W8wMainForm_ClientSizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                //// 最小化時はタスクバーに表示しない。
                this.ShowInTaskbar = false;
            }
        }

        /// <summary>
        /// [Appイベント] ゲームが終了した
        /// </summary>
        private void OnGameInactive()
        {
            this.osdForm.HideStatus();

            this.notifyIcon1.Icon = w8w.Properties.Resources.W8w;
            this.Text = Title;
        }

        /// <summary>
        /// [Appイベント] ゲームが一時停止した
        /// </summary>
        private void OnGamePause()
        {
            this.osdForm.ShowStatus(this.gameWatcher.Point);

            this.notifyIcon1.Icon = w8w.Properties.Resources.W8wSleep;
            this.Text = $"{Title} - {this.processName}:Paused.";
        }

        /// <summary>
        /// [Appイベント] ゲームが再開した
        /// </summary>
        private void OnGameResume()
        {
            this.osdForm.HideStatus();

            this.notifyIcon1.Icon = w8w.Properties.Resources.W8wActive;
            this.Text = $"{Title} - {this.processName}:Running.";
        }
    }
}
#pragma warning restore SA1300 // Element must begin with upper-case letter
