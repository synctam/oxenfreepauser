﻿namespace W8w
{
    partial class W8wMainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelPause = new System.Windows.Forms.Label();
            this.labelActive = new System.Windows.Forms.Label();
            this.labelIdol = new System.Windows.Forms.Label();
            this.pictureBoxPause = new System.Windows.Forms.PictureBox();
            this.pictureBoxActive = new System.Windows.Forms.PictureBox();
            this.pictureBoxIdol = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIdol)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Text = "Oxenfree 一時停止ツール";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.Click += new System.EventHandler(this.notifyIcon1_Click);
            this.notifyIcon1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExit});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(94, 26);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(93, 22);
            this.mnuExit.Text = "E&xit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(415, 261);
            this.textBox1.TabIndex = 0;
            this.textBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.labelPause);
            this.groupBox1.Controls.Add(this.labelActive);
            this.groupBox1.Controls.Add(this.labelIdol);
            this.groupBox1.Controls.Add(this.pictureBoxPause);
            this.groupBox1.Controls.Add(this.pictureBoxActive);
            this.groupBox1.Controls.Add(this.pictureBoxIdol);
            this.groupBox1.Location = new System.Drawing.Point(12, 279);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(415, 136);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "タスク トレイ アイコンの説明";
            // 
            // labelPause
            // 
            this.labelPause.AutoSize = true;
            this.labelPause.Location = new System.Drawing.Point(44, 103);
            this.labelPause.Name = "labelPause";
            this.labelPause.Size = new System.Drawing.Size(60, 12);
            this.labelPause.TabIndex = 12;
            this.labelPause.Text = "labelPause";
            // 
            // labelActive
            // 
            this.labelActive.AutoSize = true;
            this.labelActive.Location = new System.Drawing.Point(44, 66);
            this.labelActive.Name = "labelActive";
            this.labelActive.Size = new System.Drawing.Size(62, 12);
            this.labelActive.TabIndex = 11;
            this.labelActive.Text = "labelActive";
            // 
            // labelIdol
            // 
            this.labelIdol.AutoSize = true;
            this.labelIdol.Location = new System.Drawing.Point(44, 29);
            this.labelIdol.Name = "labelIdol";
            this.labelIdol.Size = new System.Drawing.Size(47, 12);
            this.labelIdol.TabIndex = 10;
            this.labelIdol.Text = "labelIdol";
            // 
            // pictureBoxPause
            // 
            this.pictureBoxPause.Location = new System.Drawing.Point(6, 94);
            this.pictureBoxPause.Name = "pictureBoxPause";
            this.pictureBoxPause.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxPause.TabIndex = 9;
            this.pictureBoxPause.TabStop = false;
            // 
            // pictureBoxActive
            // 
            this.pictureBoxActive.Location = new System.Drawing.Point(6, 56);
            this.pictureBoxActive.Name = "pictureBoxActive";
            this.pictureBoxActive.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxActive.TabIndex = 8;
            this.pictureBoxActive.TabStop = false;
            // 
            // pictureBoxIdol
            // 
            this.pictureBoxIdol.Location = new System.Drawing.Point(6, 18);
            this.pictureBoxIdol.Name = "pictureBoxIdol";
            this.pictureBoxIdol.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxIdol.TabIndex = 7;
            this.pictureBoxIdol.TabStop = false;
            // 
            // W8wMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 427);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox1);
            this.MaximizeBox = false;
            this.Name = "W8wMainForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.W8wMainForm_FormClosing);
            this.Load += new System.EventHandler(this.W8wMainForm_Load);
            this.ClientSizeChanged += new System.EventHandler(this.W8wMainForm_ClientSizeChanged);
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIdol)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelPause;
        private System.Windows.Forms.Label labelActive;
        private System.Windows.Forms.Label labelIdol;
        private System.Windows.Forms.PictureBox pictureBoxPause;
        private System.Windows.Forms.PictureBox pictureBoxActive;
        private System.Windows.Forms.PictureBox pictureBoxIdol;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
    }
}

