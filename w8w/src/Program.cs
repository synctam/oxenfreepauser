﻿namespace W8w
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Security.AccessControl;
    using System.Security.Principal;
    using System.Threading;
    using System.Windows.Forms;
    using Win32Native;

    internal static class Program
    {
        private const string MutexName =
            "Global\\AFAFBDE1-AB92-4F59-A9BB-5AD45B3DDD04";

        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        private static void Main()
        {
            bool hasHandle = false;
            //// 別ユーザーで起動されたプロセスの mutex も認識するようにする。
            MutexAccessRule rule = new MutexAccessRule(
                new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                MutexRights.FullControl,
                AccessControlType.Allow);
            MutexSecurity mutexSecurity = new MutexSecurity();
            mutexSecurity.AddAccessRule(rule);
            bool createdNew;
            var mutex = new Mutex(false, MutexName, out createdNew, mutexSecurity);
            try
            {
                try
                {
                    hasHandle = mutex.WaitOne(0, false);
                }
                catch (AbandonedMutexException)
                {
                    //// 既に起動中のプロセスが強制終了した場合など
                    hasHandle = true;
                }

                if (hasHandle == false)
                {
                    MessageBox.Show("多重起動はできません。");
                    ActivatePrevWindow();
                    return;
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new W8wMainForm());
            }
            finally
            {
                if (hasHandle)
                {
                    mutex.ReleaseMutex();
                }

                mutex.Close();
            }
        }

        /// <summary>
        /// すでに起動中の場合は、起動中のウィンドウをアクティブにする。
        /// ToDo: 最小化されている場合は、解除する。
        /// </summary>
        private static void ActivatePrevWindow()
        {
            var processName = Path.GetFileNameWithoutExtension(Process.GetCurrentProcess().MainModule.FileName);
            var hWnd = Win32API.GetPrevGameWindowHandle(processName);
            //// 最小化を解除する処理がうまく動作していない。
            Win32API.Restore(hWnd);
            //// 起動中のウィンドウをアクティブにする。
            Win32API.SetForceForegroundWindow(hWnd);
            //// 起動中のウィンドウを前面に表示する。
            Win32API.BringWindowToTopActivation(hWnd);
        }
    }
}
