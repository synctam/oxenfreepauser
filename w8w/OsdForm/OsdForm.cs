﻿#pragma warning disable SA1300 // Element must begin with upper-case letter
namespace w8w.OsdForm
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using Win32Native;

    /// <summary>
    /// OSDフォーム
    /// 1."一時停止"を表示するフォーム。
    /// 2.フォームは透過色を設定し文字のみが見えるようにする。
    /// 3.表示/非表示の切り替えは文字列の有無で行う。
    /// 4.ゲームが起動されたら親ウィンドウをゲーム画面に変更する。
    /// 5.ゲームが終了したら親ウィンドウをデフォルトに戻す。
    /// </summary>
    public partial class OsdForm : Form
    {
        /// <summary>
        /// "一時停止" の表示位置
        /// </summary>
        private Point labelOffset = new Point(8, 8);

        internal OsdForm()
        {
            this.InitializeComponent();
            this.Init();
        }

        /// <summary>
        /// 初期化
        /// </summary>
        internal void Init()
        {
            //// フォントサイズは設定ファイルの値を仕様する。
            this.StatusLabel.Font = new Font(
                this.StatusLabel.Font.OriginalFontName,
                Properties.Settings.Default.OsdFontSize,
                FontStyle.Regular);
            this.StatusLabel.Top = 0;
            this.StatusLabel.Left = 0;
            //// 起動時は文字列を空にする。
            this.StatusLabel.Text = string.Empty;

            //// 透過色の設定
            this.TransparencyKey = this.BackColor;
            //// フォームサイズの設定
            this.ClientSize = new Size(this.StatusLabel.Width, this.StatusLabel.Height);
            //// フォームのスタイルは枠なし
            this.FormBorderStyle = FormBorderStyle.None;
            //// タスクバーに表示しない
            this.ShowInTaskbar = false;
        }

        /// <summary>
        /// 状態を表示する。
        /// </summary>
        /// <param name="point">フォームの表示位置</param>
        internal void ShowStatus(Win32API.POINT point)
        {
            this.StatusLabel.Text = "一時停止";
            this.Left = point.X + this.labelOffset.X;
            this.Top = point.Y + this.labelOffset.Y;
        }

        /// <summary>
        /// 状態の表示を消す。
        /// </summary>
        internal void HideStatus()
        {
            this.StatusLabel.Text = string.Empty;
        }
    }
}
#pragma warning restore SA1300 // Element must begin with upper-case letter
